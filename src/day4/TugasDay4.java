package day4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class TugasDay4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

	//No.1
//		System.out.print("Jam masuk = ");
//		String jamMasuk = input.next().trim();
//		String jamM = jamMasuk.substring(0, 2);
//		String menitM = jamMasuk.substring(3);
//
//		System.out.print("Jam keluar = ");
//		String jamKeluar = input.next().trim();
//		String jamK = jamKeluar.substring(0, 2);
//		String menitK = jamKeluar.substring(3);
//
//		// semuanya diganti menjadi integer
//
//		int jamMasukInt = Integer.parseInt(jamM);
//		int menitMasukInt = Integer.parseInt(menitM);
//		int jamKeluarInt = Integer.parseInt(jamK);
//		int menitKeluarInt = Integer.parseInt(menitK);
//
//		// dikonversi ke detik
//		int waktuMasuk = (3600 * jamMasukInt) + (60 * menitMasukInt);
//		int waktuKeluar = (3600 * jamKeluarInt) + (60 * menitKeluarInt);
//
//		int totalDetik = waktuKeluar - waktuMasuk;
//		int totalJam = totalDetik / 3600;
//		int sisa = totalDetik % 3600;
//		int menit = sisa / 60;
//
//		System.out.println("Lama parkir = " + totalJam + " jam " + menit + " menit ");
//		
//		
//		if (menit > 30) {
//			totalJam = totalJam + 1;
//		} 
//
//		int tarif = totalJam * 3000;
//
//		System.out.println("Tarif = Rp. " + tarif);
//		
		
	//No.2
		
//		System.out.print("dateFormat peminjaman = ");
//		String pinjam = input.next();
//		System.out.print("Lama peminjaman = ");
//		int lama = input.nextInt();
//		System.out.print("dateFormat pengembalian = ");
//		String kembali = input.next();
//		
//		
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//		
//		Date peminjaman = null ;
//		Date pengembalian = null;
//		
//		try {
//			peminjaman = dateFormat.parse(pinjam);
//			pengembalian = dateFormat.parse(kembali);
//			
//			long diff = pengembalian.getTime() - peminjaman.getTime();
//			
//			long diffDays = diff / (24 * 60 * 60 * 1000);
//			
//			long telat = diffDays - lama ;
//			
//			long denda = telat * 500 ;
//			
//			System.out.println("Denda = Rp. " + denda);
//		} catch (ParseException z ) {
//			z.printStackTrace();
//			
//		}
		
		
 //no 1
//		System.out.print("Waktu Masuk = ");
//		String masuk = input.next();
//		input.nextLine();
//		System.out.print("Waktu keluar = ");
//		String keluar = input.next();
//		
//		SimpleDateFormat jam = new SimpleDateFormat ("dd-MM-yyyy hh:mm");
//		
//		Date masukParkiran = null ;
//		Date keluarParkiran = null;
//		
//		try {
//			masukParkiran = jam.parse(masuk);
//			keluarParkiran = jam.parse(keluar);
//		} catch (ParseException z ) {
//			z.printStackTrace();
//		}
//		
//		double diff = masukParkiran.getTime() - keluarParkiran.getTime();
//		double diffHours = diff / (60 * 60 * 1000);
//	//	cara 1 merubah double to string
//	//	int sumParse = Integer.parseInt(String.valueOf(diffHours));
//	//	cara 2 merubah double to string
//		int sumParse = (int) Math.ceil(diffHours);
//		int tarif = sumParse * 3000;
//		input.close();
//		
//		System.out.println(tarif);
		
		
		
		//no3
		
		System.out.print("Mulai kelas : ");
		String inputTanggalMulai = input.nextLine();
		System.out.print("Lama belajar : ");
		int lama = input.nextInt();
		System.out.print("libur : ");
		String[] libur = input.next().split(",");
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh MMMM yyyy");	
		
		String nextDate ;
		
		try {
			
			
			Calendar kalender = Calendar.getInstance(); //mengakses objek kalender
			kalender.setTime(dateFormat.parse(inputTanggalMulai));
			
		
			int tempHitungLibur = 0;
			
			int i = 0 ;
			while (i < lama + tempHitungLibur) {
				kalender.add(Calendar.DATE, 1);
				int day = kalender.getTime().getDay();
				int dateOfHoliday = kalender.getTime().getDate();
				
				int j = 0 ;
				if ((day == 6) || (day == 0)) {
					tempHitungLibur++;
				}
				while (j < libur.length) {
					if (dateOfHoliday == Integer.parseInt(libur[j])) {
						tempHitungLibur++;
					}
					j++;
				}
				i++;
			}	
			
			System.out.println("Total libur = " + (tempHitungLibur + libur.length));
			
			nextDate = dateFormat.format(kalender.getTime());
			System.out.println("Tanggal selesai = " + nextDate);
			
		} catch (ParseException z) {
			z.printStackTrace();
		}
		
				
		
		
		
		
		
		
		//n0 4
		
//		System.out.println("Masukkan kata : ");
//		String kata = input.next();
//		
//		int banyak = kata.length();
//		char[] huruf = kata.toCharArray();
//		
//		String balik = "";
//		for (int i = banyak-1; i >= 0 ; i--) {
//			balik = balik + huruf[i];
//		} if (kata.equals(balik)) {
//			System.out.println("yes");
//		} else {
//			System.out.println("no");
//		}
//		
		
	}

}
