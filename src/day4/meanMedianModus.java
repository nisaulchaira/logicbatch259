package day4;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

public class meanMedianModus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		DecimalFormat z = new DecimalFormat("#.##");

		System.out.println("Banyak data : ");
		int n = input.nextInt();

		double[] nilai = new double[n];
		double mean = 0;
		double median = 0;
		double modus = 0;
		double jumlah = 0;

		System.out.println("Masukkan data : ");
		for (int i = 0; i < n; i++) {
			int x = input.nextInt();
			nilai[i] = x;
		}

		Arrays.sort(nilai);;
		System.out.println(Arrays.toString(nilai));

		// menghitung mean
		for (int i = 0; i < n; i++) {
			jumlah = jumlah + nilai[i];
		}

		mean = jumlah / n;

		// menentukan median
		if (n % 2 == 1) {
			median = nilai[((n - 1) / 2)];
		} else if (n % 2 == 0) {
			median = ((nilai[(n / 2) - 1] + nilai[n / 2]) / 2);
		}

		// menentukan modus

		int max = 0;
		for (int i = 0; i < n; i++) {
			int count = 0;
			for (int j = 0; j < n; j++) {
				if (nilai[j] == nilai[i]) {
					count++;
				}
				if (count > max) {
					max = count;
					modus = nilai[i];
				}
			}
		}

		System.out.println("Mean   = " + z.format(mean));
		System.out.println("Median = " + z.format(median));
		System.out.println("Modus  = " + modus);
	}

}
