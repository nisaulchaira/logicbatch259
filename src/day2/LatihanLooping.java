package day2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class LatihanLooping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n = ");
		int n = input.nextInt();

		int x = 1 ;
		
		int[] tempArray = new int[n]; //deklarasi array integer sepanjang n baris
		
//		String[] tempArrayString = new String[5]; //deklarasi array string sepanjang 5 baris
		
		// mengisi array
		for (int i = 0 ; i < tempArray.length ; i++) {
			tempArray[i] = x ;
			x++;
		}
		
		//menampilkan isi array
		for (int i = 0; i < tempArray.length ; i++) {
			System.out.print(tempArray[i] + " ");
		}
		
//		System.out.println(Arrays.toString(tempArray)); //mencetak semua isi array
	
		
		
		
//		for (int i = 0; i < n; i += 1) { // i++ artinya increment, bisa juga ditulus i+=1
//			System.out.print(x + " ");
//			x++; 
//		}
//					
//			
//			if (i % 2 == 0) { // i modulus 2 = 0,  artinya jika i dibagi 2 sisanya 0
//				System.out.print(i + " ");
//			}

	}

}
