package Strings;

import java.util.Scanner;

public class CamelCase01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan kalimat : ");
		String s = input.nextLine();
		
		char[] huruf = s.toCharArray();
		
		int jumlah = 0 ;
		for (int i = 0; i < s.length(); i++) {
			if (Character.isUpperCase(huruf[i])) { //Jika ada karakter yg Uppercase pada huruf[i]
				jumlah++ ;
			}
		}
		
		System.out.println("Banyak kata = " + (jumlah + 1));

	}

}
