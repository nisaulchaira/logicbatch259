package Strings;

import java.util.Scanner;

public class Gemstone08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int n = input.nextInt();
		
		String huruf = "";
		int[] jumlah = new int[26];
		int[] temp ;
		
		for (int i = 0; i < n; i++) {
			temp = new int[26];
			huruf = input.next();
			for (int c : huruf.toCharArray()) {
				temp [c-97] ++;
				if (temp[c-97] == 1) {
					jumlah[c-97] ++;
				}
			}
			temp = null ;
		}
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			if (jumlah[i]== n) {
				sum +=1 ;
			}
		}
		jumlah = null;
		System.out.println(sum);
	}

}
