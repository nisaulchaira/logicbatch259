package Strings;

import java.util.Scanner;

public class hacherrankInAString05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("banyak kata : ");
		int z = input.nextInt();

		while (z-- > 0) {
			String kata1 = input.next();
			String kata2 = "hackerrank";

			int m = kata1.length();
			int n = kata2.length();

			char huruf1[] = kata1.toCharArray();
			char huruf2[] = kata2.toCharArray();

			int c[][] = new int[n + 1][m + 1];

			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= m; j++) {
					if (huruf2[i - 1] == huruf1[j - 1]) {
						c[i][j] = c[i - 1][j - 1] + 1;
					} else {
						c[i][j] = Math.max(c[i-1][j], c[i][j-1]);
					}
				}
			}
			
			int result = c[n][m];
			if (result == n) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}

		}
	}

}
