package Strings;

import java.util.Scanner;

public class StrongPassword02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Panjang password : ");
		int n = input.nextInt();
		String password = input.next();
		
		char[] huruf = password.toCharArray();

		int number = 0;
		int lower = 0;
		int upper = 0;
		int special = 0;
		int count = 0;

		for (int i = 0; i < password.length(); i++) {
	//		int ascii = (char) password.charAt(i); 
			if (huruf[i] >= 48 && huruf[i] <= 57) {
				number++;
			} else if (huruf[i] >= 65 && huruf[i] <= 90) {
				upper++;
			} else if (huruf[i] >= 97 && huruf[i] <= 122) {
				lower++;
			} else {
				special++;
			}
		}

		if (number == 0) {
			count++;
		}
		if (upper == 0) {
			count++;
		}
		if (lower == 0) {
			count++;
		}
		if (special == 0) {
			count++;
		}
		
		if (password.length() >= 6) {
			System.out.println(count);
		} else {
			System.out.println(count + (6-(password.length()+count)));
		}

	}

}
