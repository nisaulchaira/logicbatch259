package Strings;

import java.util.Scanner;

public class pangrams06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan kalimat : ");
		String s = input.nextLine();
		
		
		for (int i = 0; i < 26; i++) {
			int asciiLower = (char)'a';
			int asciiUpper = (char)'A';
			for (int j = 0; j < s.length(); j++) {
				if (s.charAt(j) == (asciiLower+i) || s.charAt(j) == (asciiUpper+i) ) {
					break ;
				} else if (j == s.length()-1 && (s.charAt(j) != (asciiLower+i) ||
						s.charAt(j) != (asciiUpper + i))) {
					System.out.println("Not Pangram");
				}
			}
		}
		System.out.println("Pangram");
	}

}
