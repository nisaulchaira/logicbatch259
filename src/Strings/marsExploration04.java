package Strings;

import java.util.Scanner;

public class marsExploration04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan sinyal SOS : ");
		String s = input.next().toUpperCase();
		char[] huruf = s.toCharArray();

		int count = 0;

		for (int i = 0; i < s.length(); i = i + 3) {
			if (huruf[i] != 'S' || (huruf[i + 1] != 'O') || (huruf[i + 2] != 'S')) {
				count++;
			}
		}
		System.out.println(count);
	}

}
