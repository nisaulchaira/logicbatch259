package Strings;

import java.util.Scanner;

public class caesarCipher03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

//		System.out.print("Banyak huruf : ");
//		int n = input.nextInt();
		System.out.print("Text awal : ");
		String textAwal = input.nextLine();
		System.out.print("Key : ");
		int key = input.nextInt();

		String akhir = "";
		char huruf;

		for (int i = 0; i < textAwal.length(); i++) {
			huruf = textAwal.charAt(i);
			if (huruf >= 'a' && huruf <= 'z') {
				huruf = (char) (huruf + key);
				if (huruf > 'z') {
					huruf = (char) (huruf + ('a' - 'z' - 1));
				}
				akhir = akhir + huruf;
			} else if (huruf >= 'A' && huruf <= 'Z') {
				huruf = (char) (huruf + key);
				if (huruf > 'Z') {
					huruf = (char) (huruf + ('A' - 'Z' - 1));
				}
				akhir = akhir + huruf ;
			} else {
				akhir = akhir + huruf ;
			}
		}
		
		System.out.println("Text akhir : " + akhir );
	}

}
