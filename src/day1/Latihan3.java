package day1;

import java.util.Scanner;

public class Latihan3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Belanja         = ");
		int belanja = input.nextInt();
		System.out.print("Ongkos Kirim    = ");
		int ongkir = input.nextInt();

		int potonganOngkir = 0;
		int potonganHarga = 0;
		if (belanja >= 100000) {
			if (ongkir >= 10000) {
				potonganOngkir = 10000;
				potonganHarga = 20000;
			} else {
				potonganOngkir = ongkir;
				potonganHarga = 20000;
			}
		} else if (belanja >= 50000) {
			if (ongkir >= 10000) {
				potonganOngkir = 10000;
				potonganHarga = 10000;
			} else {
				potonganOngkir = ongkir;
				potonganHarga = 10000;
			}
		} else if (belanja >= 30000) {
			if (ongkir >= 5000) {
				potonganOngkir = 5000;
				potonganHarga = 5000;
			} else {
				potonganOngkir = ongkir;
				potonganHarga = 5000;
			}
		}
		int total = belanja + ongkir - potonganOngkir - potonganHarga;

		System.out.println("Belanja         = " + belanja);
		System.out.println("Ongkos kirim    = " + ongkir);
		System.out.println("Diskon Ongkir   = " + potonganOngkir);
		System.out.println("Diskon Belanja  = " + potonganHarga);
		System.out.println("Total Belanja   = " + total);
	}
}
