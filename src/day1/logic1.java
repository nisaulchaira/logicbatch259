package day1;

import java.util.Scanner;

public class logic1 {
	int id = 1;
	String nama = "Nisa";

	int a = 10;
	int b = 20;
	boolean salah = false;

	int arr[] = { 10, 20, 30 };

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		logic1 lc = new logic1(); // lc=penamaan objek

		System.out.println("Nama : " + lc.nama);

		System.out.println(lc.a + lc.b);
		System.out.println(lc.salah);

		double seribu = 1000.0;
		double duaribu = 2000.0;

		System.out.println(seribu + duaribu);

		String strA = "5";
		String strB = "3"; // S nya harus huruf besar

		System.out.println(Integer.parseInt(strA) + Integer.parseInt(strB)); // merubah string menjadi integer

		Scanner input = new Scanner(System.in); // mengakses sistem yg ada di dalam java
		System.out.println("Masukkan Nilai 1: ");
		int numberOne = input.nextInt();

		System.out.println("Masukkan Nilai 2: ");
		int numberTwo = input.nextInt();

		int jumlah = numberOne + numberTwo;

		// System.out.println("Jumlah = " + (numberOne + numberTwo)); //dikasih kurung
		// biar bsa dijumlahkan

		System.out.println("Hasil = " + jumlah);

	}
}
