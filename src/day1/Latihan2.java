package day1;

import java.util.Scanner;

public class Latihan2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Belanja        : ");
		int belanja = input.nextInt();
		System.out.print("Jarak          : ");
		int jarak = input.nextInt();
		System.out.print("Masukkan Promo : ");
		String promo = input.next();

		int diskon = 0;
		int ongkir = 0;

		if (belanja >= 30000) {
			if (promo.equals("JKTOVO")) {
				diskon = belanja * 40 / 100;
			} else {
				System.out.println("Kode Promo Salah !");
			}
		} else {
			System.out.println("Tidak ada potongan harga");
		}

		if (jarak <= 5) {
			ongkir = 5000;
		} else {
			ongkir = jarak * 1000;
		}

		if (diskon >= 30000) {
			diskon = 30000;
		}
		System.out.println("Belanja        = " + belanja);
		System.out.println("Diskon 40%     = " + diskon);
		System.out.println("Ongkir         = " + ongkir);
		System.out.println("Total Belanja  = " + (belanja - diskon + ongkir));

	}
}
