package WarmUp;

import java.util.Scanner;

public class compareTheTriplets10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int banyak = 3 ;
		int[] a = new int[banyak];
		int[] b = new int[banyak];
		
		System.out.print("Alice = ");
		for (int i = 0; i < banyak; i++) {
			int x = input.nextInt();
			a[i] = x ;
		}
		
		System.out.print("Bob = ");
		for (int i = 0; i < banyak; i++) {
			int y = input.nextInt();
			b[i] = y ;
		}
		
		int nilaiA = 0 ;
		int nilaiB = 0 ;
		
		for (int i = 0; i < banyak; i++) {
			if ( a[i] > b[i]) {
				nilaiA++ ;
			} else if (b[i] > a[i]) {
				nilaiB++ ;
			}
		}
		
		System.out.println(nilaiA + " " + nilaiB);
	}

}
