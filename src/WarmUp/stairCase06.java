package WarmUp;

import java.util.Scanner;

public class stairCase06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		int n = input.nextInt();
		String[][] s = new String[n][n];

		for (int i = 0; i < s.length; i++) {
			for (int j = 0; j < s.length; j++) {
				if ((j+i)>=(n-1)) {
					s[i][j] = "#";	
				} else {
					s[i][j] = " "; 
				}
				System.out.print(s[i][j] + " ");
			}
			System.out.println();
		}
	}

}
