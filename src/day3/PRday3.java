package day3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class PRday3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		// soal 01

//		System.out.print("Banyaknya baris = ");
//		int n = input.nextInt();
//		
//		int[] arr = new int [n];
//				
//		arr[0] = 1;
//		arr[1] = 1;
//		for (int i = 2; i < n; i++) {
//			arr[i] = (arr[i-1] + arr [i-2]) ;
//		}
//		
//		for (int i = 0; i < n; i++) {
//			System.out.print(arr[i] + " ");
//		}

		// soal 02
//		System.out.print("Banyaknya baris = ");
//		int n = input.nextInt();
//
//		int[] arr = new int[n];
//
//		for (int i = 0; i < 3; i++) {
//			arr[i] = 1;
//		}
//
//		for (int i = 3; i < n; i++) {
//			arr[i] = (arr[i - 1] + arr[i - 2] + arr[i - 3]);
//		}
//
//		for (int i = 0; i < n; i++) {
//			System.out.print(arr[i] + " ");
//		}

		// soal 03

//		System.out.print("Bilangan prima dibawah ");
//		int n = input.nextInt();
//
//		for (int i = 1; i < n; i++) {
//			int angka = 0;
//			for (int j = 1; j <= i; j++) {
//				if (i % j == 0) {
//					angka = angka + 1;
//				}
//			}
//			if (angka == 2) {
//				System.out.print(i + " ");
//			}
//		}

		// soal 04

//		System.out.print("input waktu ");
//		String x = input.nextLine().toLowerCase();
//		
//		DateFormat awal = new SimpleDateFormat("hh:mm:ssaa");
//		DateFormat akhir = new SimpleDateFormat("HH:mm:ss");
//		Date date = null;
//		String output = null;
//		try {
//			date = awal.parse(x);
//			output = akhir.format(date);
//			System.out.println(output);
//		} catch (ParseException z) {
//			z.printStackTrace();
//		}

		// soal 05

		System.out.print("Menentukan pohon faktor dari ");
		int n = input.nextInt();
		int x = 0;
		String result = "";

		for (int i = 2; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				if (n % i == 0) {
					x = n / i;
					System.out.println(n + "/" + i + "=" + x);
					n = x;
				}
			}
		}

	}

}
